/**
 * Soal No. 1 (Callback Baca Buku)
 */
// di index.js
var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
readBooks(10000, books[0], function(remaining){
    readBooks(remaining, books[1], function (remaining2) {
        readBooks(remaining2, books[2], function (remaining3) {
            console.log('Selesai')
        })
    })
})
