/**
 * Soal No. 2 (Promise Baca Buku)
 */

var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise
readBooksPromise(10000, books[0])
    .then(function (remaining) {
        readBooksPromise(remaining, books[1])
            .then(function(remaining2){
                readBooksPromise(remaining2, books[2])
                    .then(function(remaining3){
                        console.log('Selesai')
                    })
                    .catch(function (error) {
                        console.log(error.message);
                    })
            })
            .catch(function (error) {
                console.log(error.message);
            })
    })
    .catch(function (error) {
        console.log(error.message);
    })
