/**
 * If-else
 */
var nama = "John"
var peran = ""

if (nama.trim() == '') {
    console.log("Nama harus diisi!")
}

if (nama.trim() != '') {
    if (peran.trim() == '') {
        console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
    }

    if (peran.trim().toLowerCase() == 'penyihir') {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo Penyihir " + nama +", kamu dapat melihat siapa yang menjadi werewolf!")
    }

    if (peran.trim().toLowerCase() == 'guard') {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo Guard " + nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }

    if (peran.trim().toLowerCase() == 'werewolf') {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo Werewolf " + nama +", Kamu akan memakan mangsa setiap malam!")
    }

    if (
        peran.trim().toLowerCase() != '' &&
        peran.trim().toLowerCase() != 'penyihir' &&
        peran.trim().toLowerCase() != 'guard' &&
        peran.trim().toLowerCase() != 'werewolf'
        ) {
        console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
    }
}

/**
 * Switch Case
 */
var hari = 17;
var bulan = 8;
var tahun = 1945;
var nama_bulan = '';

switch(bulan) {
  case 1:   { nama_bulan = 'Januari'; break; }
  case 2:   { nama_bulan = 'Februari'; break; }
  case 3:   { nama_bulan = 'Maret'; break; }
  case 4:   { nama_bulan = 'April'; break; }
  case 5:   { nama_bulan = 'Mei'; break; }
  case 6:   { nama_bulan = 'Juni'; break; }

  case 7:   { nama_bulan = 'Juli'; break; }
  case 8:   { nama_bulan = 'Agustus'; break; }
  case 9:   { nama_bulan = 'September'; break; }
  case 10:  { nama_bulan = 'Oktober'; break; }
  case 11:  { nama_bulan = 'November'; break; }
  case 12:  { nama_bulan = 'Desember'; break; }
  default:  { nama_bulan = ''; }
}

var output = `${hari} ${nama_bulan} ${tahun}`
console.log(output)
