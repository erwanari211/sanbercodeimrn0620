/**
 * No. 1 Looping While
 */


$loop1Status = false;
$loop2Status = false;
$loop1 = 0;
$loop2 = 20;

while(!$loop1Status || !$loop2Status){
    if ($loop1 <= 20) {
        while($loop1 <= 20) {
            if ($loop1 == 0) {
                console.log('LOOPING PERTAMA');
                $loop1 = $loop1 + 2;
                continue;
            }

            if ($loop1 != 0) {
                console.log($loop1 + ' - I love coding');
            }
            $loop1 = $loop1 + 2;
        }
    } else {
        $loop1Status = true;
    }

    if ($loop1Status && $loop2 >= 0) {
        while($loop2 > 0) {
            if ($loop2 == 20) {
                console.log('LOOPING KEDUA');
            }

            if ($loop2 > 0) {
                console.log($loop2 + ' - I will become a mobile developer');
            }
            $loop2 = $loop2 - 2;
        }
    } else {
        $loop2Status = true;
    }
}


/**
 * No. 2 Looping menggunakan for
 */

for (var i = 1; i <= 20; i++) {
    if (i % 2 == 1) {
        if (i % 3 == 0) {
            console.log(i + ' - I Love Coding')
        } else {
            console.log(i + ' - Santai')
        }
    }

    if (i % 2 == 0) {
        console.log(i + ' - Berkualitas')
    }
}


/**
 * No. 3 Membuat Persegi Panjang
 */

var panjang = 8
var lebar = 4
for (var i = 0; i < lebar; i++) {
    var output = ''
    for (var j = 0; j < panjang; j++) {
        output += '#'
    }
    console.log(output)
}


/**
 * No. 4 Membuat Tangga
 */

var tinggi = 7
for (var baris = 1; baris <= tinggi; baris++) {
    var output = ''
    for (var kolom = 1; kolom <= baris; kolom++) {
        output += '#'
    }
    console.log(output)
}


/**
 * No. 5 Membuat Papan Catur
 */

var sisi = 8
for (var baris = 1; baris <= sisi; baris++) {
    var barisGenap = (baris % 2 == 0)
    var output = ''
    for (var kolom = 1; kolom <= sisi; kolom++) {
        var kolomGenap = (kolom % 2 == 0)
        if (barisGenap) {
            if (kolomGenap) {
                output += ' '
            } else {
                output += '#'
            }
        }

        if (!barisGenap) {
            if (!kolomGenap) {
                output += ' '
            } else {
                output += '#'
            }
        }
    }
    console.log(output)
}
