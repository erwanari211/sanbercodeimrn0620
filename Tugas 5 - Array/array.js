/**
 * Soal No. 1 (Range)
 */
function range(startNum = '', finishNum = '') {
    if (startNum == '' || finishNum == '') {
        return -1
    }

    if (startNum < finishNum) {
        let result = [];
        for (var i = startNum; i <= finishNum; i++) {
            result.push(i)
        }
        return result
    }

    if (startNum > finishNum) {
        let result = [];
        for (var i = startNum; i >= finishNum; i--) {
            result.push(i)
        }
        return result
    }

    return -1
}

console.log('=== SOAL 1 ===')
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1
console.log('')

/**
 * Soal No. 2 (Range with Step)
 */
function rangeWithStep(startNum = '', finishNum = '', step = 1) {
    if (startNum == '' || finishNum == '') {
        return -1
    }

    if (startNum < finishNum) {
        let result = [];
        for (var i = startNum; i <= finishNum; i = i + step) {
            result.push(i)
        }
        return result
    }

    if (startNum > finishNum) {
        let result = [];
        for (var i = startNum; i >= finishNum; i = i - step) {
            result.push(i)
        }
        return result
    }

    return -1
}

console.log('=== SOAL 2 ===')
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log('')

/**
 * Soal No. 3 (Sum of Range)
 */
function sum(startNum = '', finishNum = '', step = 1) {
    if (startNum == '') {
        return 0
    }

    if (startNum) {
        if (finishNum == '') {
            return startNum
        }
    }

    let arrayNum = rangeWithStep(startNum, finishNum, step)
    let result = 0
    for (var i = 0; i < arrayNum.length; i++) {
        result += arrayNum[i]
    }

    return result
}

console.log('=== SOAL 3 ===')
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log('')

/**
 * Soal No. 4 (Array Multidimensi)
 */
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(data) {
    for (var i = 0; i < data.length; i++) {
        let id = data[i][0]
        let name = data[i][1]
        let address = data[i][2]
        let dob = data[i][3]
        let hobby = data[i][4]

        console.log(`Nomor ID:  ${id}`);
        console.log(`Nama Lengkap:  ${name}`);
        console.log(`TTL:  ${address} ${dob}`);
        console.log(`Hobi:  ${hobby}`);
        console.log(``)
    }
}
console.log('=== SOAL 4 ===')
dataHandling(input)
console.log('')

/**
 * Soal No. 5 (Balik Kata)
 */
function balikKata(str) {
    let result = ''
    for (var i = str.length - 1; i >= 0; i--) {
        result += str[i]
    }

    return result
}

console.log('=== SOAL 5 ===')
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log('')

/**
 * Soal No. 6 (Metode Array)
 */
function dataHandling2(data){
    var result = data

    var name = data[1]
    var newName = name + ' Elsharawy'
    result.splice(1, 1, newName)

    var address = data[2]
    var newAddress = 'Provinsi ' + address
    result.splice(2, 1, newAddress)

    var gender = 'Pria'
    var school = 'SMA Internasional Metro'
    result.splice(4, 1) // remove hobby
    result.splice(4, 0, gender)
    result.splice(5, 0, school)
    console.log(result) // ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]

    var dobArray = data[3].split('/')
    var day = dobArray[0]
    var month = dobArray[1]
    var year = dobArray[2]
    var monthName = ''

    switch(month) {
        case '01':   { monthName = 'Januari'; break; }
        case '02':   { monthName = 'Februari'; break; }
        case '03':   { monthName = 'Maret'; break; }
        case '04':   { monthName = 'April'; break; }
        case '05':   { monthName = 'Mei'; break; }
        case '06':   { monthName = 'Juni'; break; }

        case '07':   { monthName = 'Juli'; break; }
        case '08':   { monthName = 'Agustus'; break; }
        case '09':   { monthName = 'September'; break; }
        case '10':  { monthName = 'Oktober'; break; }
        case '11':  { monthName = 'November'; break; }
        case '12':  { monthName = 'Desember'; break; }
        default:  { monthName = ''; }
    }
    console.log(monthName) // Mei

    var sortedDobArray = dobArray.slice()
    sortedDobArray = sortedDobArray.sort(function (value1, value2) { return value2 - value1 } )
    console.log(sortedDobArray) // ["1989", "21", "05"]

    formattedDob = dobArray.join('-')
    console.log(formattedDob) // 21-05-1989

    var slicedName = newName.slice(0, 15)
    console.log(slicedName)
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
console.log('=== SOAL 6 ===')
dataHandling2(input);
console.log('')
