import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Axios from 'axios';

export default class App extends Component {
  state = {
      data: []
  }

  // Mount User Method
  componentDidMount() { 
    this.fetchData()
  }

  fetchData = async () => {
    const response = await Axios.get(`https://cat-fact.herokuapp.com/facts`)
    this.setState({ data: response.data.all })

    // const response = await fetch(`https://cat-fact.herokuapp.com/facts`)
    // const json = await response.json()
    // this.setState({ data: json.all })
  }

  render() {
    return (
        <View>
            <FlatList
                data={this.state.data}
                keyExtractor={(x,i) => `item-${i}`}
                renderItem={({item}) => (
                    <Text style={styles.factItem}>
                        {`${item.text} (${item.upvotes})`}
                    </Text>
                )}
            />
        </View>
     )
    
    
  }
}

const styles = StyleSheet.create({
  factItem: {
    marginBottom: 10,
  },
})
