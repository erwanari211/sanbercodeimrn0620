import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Axios from 'axios';

export default class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       data: [],
//       isLoading: true,
//       isError: false
//     };
//   }
  state = {
      data: []
  }

  // Mount User Method
  componentDidMount() { 
    this.fetchData()
  }

  fetchData = async () => {
    const response = await fetch(`https://randomuser.me/api?results=10`)
    const json = await response.json()
    this.setState({ data: json.results })
  }

  render() {
    return (
        <View>
            <FlatList
                data={this.state.data}
                keyExtractor={(x,i) => i}
                renderItem={({item}) => (
                    <Text>
                        {`${item.name.first} ${item.name.last}`}
                    </Text>
                )}
            />
        </View>
     )
    
    
  }
}

const styles = StyleSheet.create({
  viewList: {
    height: 100,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#DDD',
    alignItems: 'center'
  },
  Image: {
    width: 88,
    height: 80,
    borderRadius: 40
  },
  textItemLogin: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 20,
    fontSize: 16
  },
  textItemUrl: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 12,
    marginTop: 10,
    color: 'blue'
  }
})
