import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

/*
// pada class component umumnya digunakan sintaks berikut: 
class App extends React.Component {
  render() {
    return (
      <View>
        <Button
          title="Menuju Screen Lain"
          onPress={() => this.props.navigation.navigate('NamaRute')}
        />
        <Button
          title="Menuju Screen Lain"
          onPress={() => this.props.navigation.navigate('NamaRute', {key:'value'})}
        />
        <Text>{this.props.route.params.key}</Text> 
      </View>
    )
  }
} 

// sedangkan pada function component:
function App ({route, navigation}) {
    return (
      <View>
        <Button
          title="Menuju Screen Lain"
          onPress={() => navigation.navigate('NamaRute')}
        />
        <Button
          title="Menuju Screen Lain"
          onPress={() => navigation.navigate('NamaRute', {key:'value'})}
        />
        <Text>{route.params.key}</Text> 
      </View>
    )
}  
*/

class HomeScreen extends React.Component {
  render() {
    const {navigation, route} = this.props
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Details"
          onPress={() => navigation.navigate('Details')}
        />

        <Button
          color="red"
          title="Go to Profile"
          onPress={() => navigation.navigate('Profiles')}
        />

      </View>
    )
  }
} 

// Screen Detail
class DetailsScreen extends React.Component {
  render() {
    const {navigation, route} = this.props
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
        <Button
          title="Go to Details... again"
          onPress={() => navigation.push('Details')}
        />
        <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
        <Button title="Go back" onPress={() => navigation.goBack()} />
        <Button
          title="Go back to first screen in stack"
          onPress={() => navigation.popToTop()}
        />
      </View>
    )
  }
}

// Screen Profile
class ProfilesScreen extends React.Component {
  render() {
    const {navigation, route} = this.props
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Profile Screen</Text>
        <Button
          color="black"
          title="Go to Home"
          onPress={() => navigation.navigate('Home')}
        />
        <Button title="Go back" onPress={() => navigation.goBack()} />
        <Button
          color="green"
          title="Go back to first screen in stack"
          onPress={() => navigation.popToTop()}
        />
      </View>
    );
  }
}

// Stack berguna untuk routing aplikasi
const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        {/*<Stack.Screen name="NamaRute" component={NamaRuteComponent} />*/}  
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
        <Stack.Screen name="Profiles" component={ProfilesScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
