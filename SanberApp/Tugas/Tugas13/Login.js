import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, TextInput, FlatList } from 'react-native'

// import Icon from '@expo/vector-icons/MaterialIcons'; 

export default class App extends Component {
  render() {

    return (
      <View style={styles.container}>

        <View style={styles.logo}>
          <Image
            source={require('./images/logo.png')}
            style={{width: 375, height: 116}}
          />
        </View>
     
        <View>
          <Text style={styles.pageTitle}>Login</Text>

          <View style={styles.form}>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Username / Email</Text>
              <TextInput style={styles.textInput} />
            </View>

            <View style={styles.formGroup}>
              <Text style={styles.label}>Password</Text>
              <TextInput style={styles.textInput} />
            </View>

            <View style={styles.formGroup}>
              <TouchableOpacity>
                <Text style={styles.loginButton}>Login</Text>
              </TouchableOpacity>

              <Text style={{textAlign: 'center', color: '#3EC6FF', paddingVertical: 16}}>atau</Text>

              <TouchableOpacity>
                <Text style={styles.registerButton}>Daftar</Text>
              </TouchableOpacity>
            </View>
            
          </View>
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  pageTitle: {
    color: '#003366',
    fontSize: 24,
    textAlign: 'center',
  },
  form: {
    padding: 24,
  },
  formGroup: {
    paddingBottom: 16,
  },
  label: {
    color: '#003366',
    paddingBottom: 4,
  },
  textInput: {
    height: 40,
    borderColor: '#003366',
    borderWidth: 1,
    padding: 10,
  },
  registerButton: {
    backgroundColor: '#003366',
    padding: 12,
    textAlign: 'center',
    color: 'white',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  loginButton: {
    backgroundColor: '#3EC6FF',
    padding: 12,
    textAlign: 'center',
    color: 'white',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  
})
