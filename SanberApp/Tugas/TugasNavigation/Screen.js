import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

export {default as LoginScreen} from './LoginScreen';
export {default as AboutScreen} from './AboutScreen'
export {default as SkillScreen} from './SkillScreen'
export {default as ProjectScreen} from './ProjectScreen'
export {default as AddScreen} from './AddScreen'
