import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, TextInput, FlatList } from 'react-native'

import Icon from '@expo/vector-icons/MaterialCommunityIcons'; 
import Skill from './components/Skill'
import SkillData from './skillData.json'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      skills: SkillData.items,
      selectedCategory: '',
    }
  }

  renderItem = ({ item }) => {
    return (
      <Skill skill={item} />
    )
  }

  displaySkill(skillCategory){
    let currentCategory = this.state.selectedCategory
    if(currentCategory === '' || currentCategory !== skillCategory){
      this.setState({selectedCategory: skillCategory})
      let filteredSkills = SkillData.items.filter((item) => {
        return item.category === skillCategory
      })
      this.setState({skills: filteredSkills})  
    } else {
      this.setState({selectedCategory: ''})
      this.setState({skills: SkillData.items})
    }
  }

  render() {

    // const extractKey = ({ id }) => id
    const extractKey = (item, index) => index.toString();

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View>
            <Image
              source={require('./../Tugas13/images/logo.png')}
              style={styles.logo}
            />
          </View> 
        </View>

        <View style={styles.profile}>
          <View style={{paddingRight: 8}}>
            <Icon name="account-circle" size={36} color="#B4E9FF" />
          </View> 

          <View>
            <Text>Hai</Text>
            <Text style={styles.username}>Erwan Ari</Text>
          </View> 
        </View>

        <View>
          <Text style={styles.pageHeader}>SKILL</Text>

          <View style={{height:3, backgroundColor:'#B4E9FF', marginVertical: 4}} />
        </View>

        <View style={styles.skillCategories}>
          <TouchableOpacity onPress={ () => this.displaySkill('Library') }  >
            <Text style={styles.skillCategory}>Library/Framework</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={ () => this.displaySkill('Language') }  >
            <Text style={styles.skillCategory}>Bahasa Pemrograman</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={ () => this.displaySkill('Technology') }  >
            <Text style={styles.skillCategory}>Teknologi</Text>
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.skills}
          renderItem={this.renderItem}
          keyExtractor={extractKey}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  }, 
  header: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  logo: {
    height: 60,
    width: 160,
    resizeMode: "contain",
  },
  profile: {
    flexDirection: 'row',
  },
  username: {
    fontWeight: 'bold',
    color: '#003366',
  },
  pageHeader: {
    fontSize: 36,
    color: '#003366',
    paddingTop: 16,
  },
  skillCategories: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 16,
  },
  skillCategory: {
    backgroundColor: '#B4E9FF',
    color: '#003366',
    fontWeight: 'bold',
    padding: 8,
  }
})
