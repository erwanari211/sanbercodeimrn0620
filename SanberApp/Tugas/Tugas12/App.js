import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, FlatList } from 'react-native'

import Icon from '@expo/vector-icons/MaterialIcons'; 
import VideoItem from './components/videoItem'; 
import data from './data.json';

export default class App extends Component {
  render() {
    console.log(data)

    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image
            source={require('./images/logo.png')}
            style={{width: 98, height: 22}}
          />

          <View style={styles.rightNav}>
            <TouchableOpacity>
              <Icon style={styles.navItem} name="search" size={24} />
            </TouchableOpacity>

            <TouchableOpacity>
              <Icon style={styles.navItem} name="account-circle" size={24} />
            </TouchableOpacity>            
          </View>
        </View>

        <View style={styles.body}>
          <FlatList 
            data={data.items}
            renderItem={(video)=><VideoItem video={video.item} />}
            keyExtractor={(item)=>item.id}
            ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor:'#E5E5E5'}} />}
          /> 

          
        </View>

        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="home" size={24} />
            <Text style={styles.TabTitle}>Home</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.tabItem}>
            <Icon name="whatshot" size={24} />
            <Text style={styles.TabTitle}>Trending</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.tabItem}>
            <Icon name="subscriptions" size={24} />
            <Text style={styles.TabTitle}>Subscriptions</Text>
          </TouchableOpacity>      

          <TouchableOpacity style={styles.tabItem}>
            <Icon name="folder" size={24} />
            <Text style={styles.TabTitle}>Library</Text>
          </TouchableOpacity>  
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',

    elevation: 3,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 0,
    paddingHorizontal: 15,

    flexDirection: 'row',
    alignItems: 'center',

    justifyContent: 'space-between',
  },
  rightNav: {
    flexDirection: 'row',
  },
  navItem: {
    marginLeft: 25,
  },
  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabTitle: {
    fontSize: 11,
    color: '#3C3C3C',
    paddingTop: 4,
  },  
})
