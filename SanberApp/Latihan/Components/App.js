import React, { Component } from 'react'

import ViewComponent from './ViewComponent' 
import TextComponent from './TextComponent' 
import ImageComponent from './ImageComponent'
import ScrollViewComponent from './ScrollViewComponent'
import FlatListComponent from './FlatListComponent'
import TouchableOpacityComponent from './TouchableOpacityComponent'
import TextInputComponent from './TextInputComponent'

export default class App extends Component {
  render() {
    return (
      <ViewComponent />
      // <TextComponent />
      // <ImageComponent />
      // <ScrollViewComponent />
      // <FlatListComponent />
      // <TouchableOpacityComponent />
      // <TextInputComponent />
    )
  }
}

