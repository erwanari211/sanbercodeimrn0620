import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Latihan from './Latihan/Components/App'
import YoutubeUI from './Tugas/Tugas12/App'
import Materi from './Materi/Materi13/index'
import Login from './Tugas/Tugas13/Login'
import Register from './Tugas/Tugas13/Register'
import AboutMe from './Tugas/Tugas13/AboutMe'
import TodoApp from './Tugas/Tugas14/App'
import SkillScreen from './Tugas/Tugas14/SkillScreen'
import Materi15 from './Materi/Materi15/index'
import NavigationExample from './Tugas/Tugas15/index'
import TugasNavigation from './Tugas/TugasNavigation/index'
import Quiz3 from './Quiz3/index'
import Materi16 from './Materi/Materi16/index'
import Latihan16 from './Materi/Materi16/latihan16'
import Latihan16b from './Materi/Materi16/latihan16b'

export default function App() {
  return (
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
    // <YoutubeUI />

    // <Register />
    // <Login />
    // <AboutMe />
    // <SkillScreen />

    // <TodoApp />
    
    // <Materi15 />
    // <NavigationExample />
    // <TugasNavigation />

    // <Quiz3 />

    // <Materi16 />
    // <Latihan16 />
    <Latihan16b />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
